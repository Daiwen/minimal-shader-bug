class Main extends hxd.App {
	private var objectScene : h2d.Scene;
	public var renderTarget : h3d.mat.Texture;
	private var bg : h2d.Bitmap;

	private var postScene : h2d.Scene;
	private var post : h2d.Bitmap;
	public var postRoot : h2d.Object;


	// Boot
	static function main() {
		hxd.Res.initEmbed();
        new Main();
	}

	// Engine ready
	override function init() {
		super.init();

		objectScene = new h2d.Scene();
		renderTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

		postScene = new h2d.Scene();
		bg = new h2d.Bitmap(h2d.Tile.fromTexture(renderTarget));
		postScene.add(bg, 1);

		createScene(objectScene);
	}

	private function createScene(s2d: h2d.Scene) {
		var sprite = new h2d.Bitmap(hxd.Res.my_sprite.toTile(), s2d);
		sprite.x = 60;
		sprite.y = 60;

		var centerX = sprite.x + 0.5 * sprite.tile.width;
		var centerY = sprite.y + 0.5 * sprite.tile.height;

		var g = new h2d.Graphics(sprite);
		g.beginFill(0xffffff, 0.);
		g.drawCircle(0, 0, 10000);
		g.endFill();

		//This draws the pivot that is used in Silhouette
		var s = new Center(engine.width, engine.height, centerX, centerY);
		g.addShader(s);

		var shader = new Silhouette(engine.width, engine.height, centerX, centerY);
		sprite.addShader(shader);

		var sprite2 = new h2d.Bitmap(hxd.Res.my_sprite.toTile(), s2d);
		sprite2.x = 60;
		sprite2.y = 60;
	}

	function render1 (e:h3d.Engine) {
		engine.pushTarget(renderTarget);

		engine.clear(0, 1);
		objectScene.render(e);

		engine.popTarget();

		postScene.render(e);
	}

	function render2 (e:h3d.Engine) {
		objectScene.render(e);
	}

	override function render (e:h3d.Engine) {
		//render1(e);
		render2(e);
	}
}

class Silhouette extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@param var width : Float;
		@param var height : Float;
		@param var center : Vec2;
		@param var scale : Float;
		@param var color : Vec4;

		function vertex() {
			var pos = vec2((outputPosition.xy + vec2(1)) * 0.5) * vec2(width, height);

			var v = pos - center;
			v = scale * v;

			var npos = center + v; 
			npos = ((npos/vec2(width, height)) * 2) - vec2(1);

			output.position = vec4(npos, outputPosition.zw);
		}

		function fragment() {
			if( pixelColor.a > 0.001 )
				output.color = color;
		}
	};


	public function new(w:Float, h:Float, centerX:Float, centerY:Float) {
		super();

		color.setColor(0xaf7f66);
		color.w = 1;

		scale = 2;

		center.x = centerX;
		center.y = centerY;

		width = w;
		height = h;
	}
}

class Center extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@param var width : Float;
		@param var height : Float;
		@param var center : Vec2;

		function fragment() {
			var pos = vec2((outputPosition.xy + vec2(1)) * 0.5) * vec2(width, height);

			var c = pos - center;
			var r = sqrt(c.x*c.x + c.y*c.y);

			if( r < 5 )
				output.color = vec4(255, 0, 0, 1);
		}
	};


	public function new(w:Float, h:Float, centerX:Float, centerY:Float) {
		super();

		center.x = centerX;
		center.y = centerY;

		width = w;
		height = h;
	}
}
